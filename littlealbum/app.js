var express = require("express");
var app = express();
//因为有入口文件
var router = require("./controller");

//模板引擎
app.set("view engine", "ejs");

//路由中间件
//静态页面
//app.use("/static", express.static("./public"));
app.use(express.static("./public"));
app.use(express.static("./uploads"));


//这里不需要传参数进去
app.get("/", router.showIndex);
app.get("/:albumName", router.showAlbum);
app.get("/up", router.showUp);
app.post("/up", router.doPost);

app.use(function(req, res){
    res.render("err");
});
app.listen(3000);