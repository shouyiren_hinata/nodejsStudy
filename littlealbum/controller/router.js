var file = require("../models/file.js");
var formidable = require('formidable');
var path = require("path");
var fs = require("fs");
var sd = require("silly-datetime");



//首页,下面这种写法错误，没有考虑到异步的操作
exports.showIndex1 = function(req, res){
    res.render("index",{
        "albums" : file.getAllAlbums()
    });
};

//下面这么写才对，上面那么写，在读写文件和数据库的时候都是不对的!!!!!!
exports.showIndex = function(req, res){

//这就是nodejs的编程思维，就是所有的东西，都是异步的。所以内层函数不是return回来东西，
//而是调用高层函数提供的回调函数，把数据当做回调函数的参数来使用
    file.getAllAlbums(function(err, allAlbums){
        if(err){
            res.render("err");
            return;
        }
        res.render("index",{
            "albums" : allAlbums
        });
    });
};

//相册页
exports.showAlbum = function(req, res, next){
    //遍历所有图片
    var albumName = req.params.albumName;
    //具体业务交给model
    file.getAllImagesByAlbumName(albumName, function(err, imagesArray){
        if(err){
            //res.render("err");
            next();
            return;
        }

        res.render("album",{
            "albumname" : albumName,
            //"images" : ["/images/404.jpg", "/images/404.jpg", "/images/404.jpg"]
            "images" : imagesArray
        });
    });

    //res.send("相册" + req.params.albumName);
};

//node中全是回调函数，所以自己封装的函数，里面如果有异步的方法，比如IO，
//就要用回调函数的方法封装

//显式上传
exports.showUp = function(req, res){

    file.getAllAlbums(function(err, albumArray){
        if(err){
            next();
            return;
        }

        res.render("up",{
            "albums" : albumArray
        });
    });

}

//上传表单
exports.doPost = function(req, res){

    var form = new formidable.IncomingForm();

    form.uploadDir = path.normalize(__dirname + "/../tempup/");
 
    form.parse(req, function(err, fields, files, next) {
        if(err){
            next();
            return;
        }

        var size = parseInt(files.tupian.size);
        if(size > 1024000){
            console.log(files.tupian);
            console.log(files.tupian.path);
            res.send("图片尺寸过大！");
            //删除文件            
            fs.unlink(files.tupian.path, function(err){

            });
            return;
        }

        var ttt = sd.format(new Date(), "YYYYMMDDHHmmss");
        var ran = parseInt(Math.random() * 89999 + 10000);
        var extname = path.extname(files.tupian.name);
        var oldpath = files.tupian.path;
        var newpath = path.normalize(__dirname + "/../uploads/" + fields.albumsOption + "/" + ttt + ran + extname);
        console.log("old:" + oldpath);                
        console.log("new:" + newpath);

        fs.rename(oldpath, newpath, function(err){
            if(err){
                res.send("改名失败");
                return;
            }
            res.send("成功");
        });
    });
}
//show dbs 查看所有数据库列表
//use itcast 使用、创建数据库
//db.student.insert({"name":"xiaoming"})； 插入向集合中插入一条文档
//db.dropDatabase() 删除当前数据库
//mongoimport --db test --collection restra --drop --file primer.json
