var express = require("express");
var app = express();
//静态服务就可以被提供
app.use(express.static("public"));

app.get("/haha", function(req, res){
    res.send("hello hahahahhahaha");
});
app.get(/^\/student\/([\d]{10})$/, function(req, res){
    res.send("学生学号：" + req.param[0]);
});

app.get("/teacher/:gonghao", function(req, res){
    res.send("老师工号：" + req.params.gonghao);
});
app.listen(3000)