var express = require("express");
var bodyParser = require("body-parser");

var app = express();


//模板引擎,在这里就引用了。
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({extended: false}));

app.get("/", function(req, res){
    res.render("haha", {
        "news" : ["001", "002", "003"]
    });
});
app.post("/", function(req, res){
    console.log(req.body);
});


app.listen(3000)