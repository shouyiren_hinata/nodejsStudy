var express = require("express");
var app = express();
var db = require("./model/db.js");

app.get("/", function(req, res){
    db.insertOne("documents", {"name":"小红"}, function(err,result){
        if(err){
            console.log("插入失败");
            return;
        }
        res.send("插入成功");
    });
});

app.get("/du1", function(req, res){
    var page = req.query.page;
    var a = [];
    //db.find("documents", {"a":3}, function(err,result){
    db.find("documents", {}, function(err,result){
        //console.log(result);
        //res.send("读取成功");
        //这种方法可以读取，但是每次都是把所有数据都读，开销很大
        for(var i = 10 * (page - 1); i < 10 * page; i++){
            if(result[i]){
                a.push(result[i]);
            } else {
                break;
            }            
        }
        //res.json({"result" : result[i]});
        console.log(a);
        res.send(a);
    });
});


//用limit和skip实现分页
//db.student.find().limit(4).skip(4);
app.get("/du2", function(req, res){
    var page = req.query.page;

    //db.find("documents", {"a":3}, function(err,result){
    db.find3("documents", {}, {"pageamount":10, "page":page}, function(err,result){
        console.log(result);
        res.send(result);
    });
});


//示例，不执行。
app.get("/shan", function(req, res){
    var restaurant_id = parseInt(req.query.id);
    db.removeMany("canguan",{"name":"xiao1"},function(err, result){
        res.send(result);
    });
});
//示例，不执行。
app.get("/xiugai", function(req, res){
    var restaurant_id = parseInt(req.query.id);
    db.removeMany("canguan",{"name":"xiao1"},{"name":"xiao2"},function(err, result){
        if(err){
            console.log(err);
        }
        
        res.send(result);
    });
});




app.listen(3000);