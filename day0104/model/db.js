//模块封装对数据库的所有操作
var MongoClient = require("mongodb").MongoClient;
const assert = require('assert');
var settings = require("../settings.js");

function _connectDB(callback){
    // Connection URL
    //const url = 'mongodb://localhost:27017/itcast';
    var url = settings.dburl;

    MongoClient.connect(url, function(err, db) {
        if(err) {
            console.log("数据库链接失败");
            return;            
        }
        //console.log("数据库链接成功");

        callback(err, db);
    });
};

exports.insertOne = function(collectionName, json, callback){
    _connectDB(function(err, client){
        var db = client.db("restra");        
        db.collection(collectionName).insertOne(json, function(err, result){
            client.close();
            callback(err, result);            
        });
    });
};

exports.find = function(dbname, collectionName, json, callback){
    var result = [];//结果数组
    //var json = json || {};
    if(arguments.length != 4)
    {
        callback("find需要4个参数", null);
        return;
    }
    _connectDB(function(err, db){
        var db = db.db(dbname);
        var cursor = db.collection(collectionName).find(json);
        cursor.each(function(err, doc){
            //assert.equal(err, null);
            if(err){
                callback(err, null);
                return;
            }
            if(doc != null) {
                //console.dir(doc);
                //没有遍历结束
                result.push(doc);
            }else{
                //遍历结束
                callback(null, result);
            }
        });

    });


}


//用skip和limit实现较为高效
exports.find2 = function(collectionName, json, args, callback){
    var result = [];//结果数组
    //var json = json || {};
    if(arguments.length != 4)
    {
        callback("find需要4个参数", null);
        return;
    }
    var skipnum = args.pageamount * args.page;
    var limit = args.pageamount;

    _connectDB(function(err, db){
        var db = db.db("restra");
        var cursor = db.collection(collectionName).find(json).skip(skipnum).limit(limit);
        cursor.each(function(err, doc){
            //assert.equal(err, null);
            if(err){
                callback(err, null);
                return;
            }
            if(doc != null) {
                //console.dir(doc);
                //没有遍历结束
                result.push(doc);
            }else{
                //遍历结束
                callback(null, result);
            }
        });

    });
}

//用skip和limit实现较为高效,一种可行的重载方案
exports.find3 = function(collectionName, json, C, D){
    var result = [];//结果数组
    //var json = json || {};
    var skipnum = 0;
    var limit = 0;
    var callback = null;
    var args = null;

    if(arguments.length == 3)
    {
        callback = C;
        skipnum = 0;
        limit = 0;
    }else if(arguments.length == 4){
        callback = D;
        args = C;
        skipnum = args.pageamount * args.page;
        limit = args.pageamount;
    }else{
        throw new Error("find函数的参数个数，必须是3/4个");
    }


    _connectDB(function(err, db){
        var db = db.db("restra");
        var cursor = db.collection(collectionName).find(json).skip(skipnum).limit(limit);
        cursor.each(function(err, doc){
            //assert.equal(err, null);
            if(err){
                callback(err, null);
                return;
            }
            if(doc != null) {
                //console.dir(doc);
                //没有遍历结束
                result.push(doc);
            }else{
                //遍历结束
                callback(null, result);
            }
        });

    });
}


//删除
exports.deleteMany = function(collectionName, json, callback){
    _connectDB(function(err, db1){
        var db = db1.db("restra");
        db.collection(collectionName).deleteMany(json, function(err, results){
            callback(err, results);
            db1.close();
        });
    });
}

//修改
exports.updateMany = function(collectionName, json1, json2, callback){
    _connectDB(function(err, db1){
        var db = db1.db("restra");
        db.collection(collectionName).updateMany(json1, json2, function(err, results){
            callback(err, results);
            db1.close();
        });
    });
}