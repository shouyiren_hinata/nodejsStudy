var express = require("express");
var app = express();
var db = require("./model/db.js");
var session = require("express-session");

app.use(session({
    secret : 'keyboard cat',
    resave : false,
    saveUninitialized : true
}));

app.set("view engine", "ejs");


app.get("/", function(req, res){
    if(req.session.login == "1"){
        res.send("welcome " + req.session.username);
    } else {
        res.send("登陆失败");
    }
});


app.get("/login",function(req, res){
    res.render("denglu");
});

app.get("/checklogin", function(req, res){
    //res.send("已经提交");
    var tianxieusername = req.query.username;
    var tianxiepassword = req.query.password;
    console.log(tianxieusername);
    db.find("itcast", "user", {"username" : tianxieusername}, function(err, result){        
        if(err){            
            res.send("查询错误！");
            return;
        }
        if(result.length == 0){            
            res.send("登录名错误，没有这个用户");
            return;
        }
        var shujvkuzzhongdepassword = result[0].password;
        if(shujvkuzzhongdepassword == tianxiepassword){
            req.session.login = "1";
            req.session.username = tianxieusername;
            res.send("成功登陆");
        } else {
            res.send("登陆失败,密码错误");
        }
    })
});

app.listen(3000);